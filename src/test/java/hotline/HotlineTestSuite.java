package hotline;

import common.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import page_object.core.ComparisonPage;
import page_object.core.FilteredProductsPage;
import page_object.core.HotlineMainPage;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class HotlineTestSuite extends BaseTest {

    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(
                new String[][]{{"Samsung Galaxy Note9 6/128GB Ocean Blue", "Samsung Galaxy S8 64GB Black"}},
                new String[][]{{"Samsung Galaxy S9 SM-G960 64GB Black", "Samsung Galaxy Note 8 64GB Black"}},
                new String[][]{{"Samsung Galaxy J6 2018 2/32GB Black", "Samsung Galaxy S8 64GB Blue"}});
    }

    @Parameterized.Parameter
    public String[] itemsToAdd;

    @Test
    public void checkComparisonFunctionality() {

        final HotlineMainPage hotlineMainPage = new HotlineMainPage(webDriver);
        hotlineMainPage.openPage(HotlineMainPage.class);

        final FilteredProductsPage filteredProductsPage = hotlineMainPage.navigateThroughTheSideBarMenu();
        filteredProductsPage.applyFilteringByBrand();
        filteredProductsPage.addItemsToComparisonList(itemsToAdd);
        filteredProductsPage.clickCompareButton();

        final Integer expectedCountOfCheckedItems = filteredProductsPage.getItemsCountAddedToCompareList();
        final Integer actualCountOfCheckedItems = itemsToAdd.length;

        Assert.assertEquals("The actual count of checked items is not as expected!",
                expectedCountOfCheckedItems, actualCountOfCheckedItems);

        final ComparisonPage comparisonPage = filteredProductsPage.proceedToComparisonPage();
        final String[] itemsAddedToComparisonList = comparisonPage.getItemsNameAddedToComparisonList();
        Arrays.sort(itemsAddedToComparisonList);
        Arrays.sort(itemsToAdd);

        Assert.assertArrayEquals("The items, added to comparison list are not as expected!",
                itemsToAdd, itemsAddedToComparisonList);

        final FilteredProductsPage filteredProductsPageAfterClearedItems = comparisonPage.clearSelectedItems();
        final String expectedEmptyComparisonItemsPopupText = "Ваш список \"Сравнения\" пуст.";
        final String actualEmptyComparisonItemsPopupText = filteredProductsPageAfterClearedItems.getEmptyComparisonItemsPopupText();

        Assert.assertEquals("The items compare popup contains incorrect text!",
                expectedEmptyComparisonItemsPopupText, actualEmptyComparisonItemsPopupText);
    }
}

package page_object.core;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static java.lang.String.*;
import static org.openqa.selenium.By.*;

public class FilteredProductsPage extends BasePage {

    @FindBy(xpath = "//div[@data-dropdown-target='compare']")
    private WebElement compareProductButton;

    @FindBy(xpath = "//div[@data-lazy-content='compare']//a[not(@data-clear-compare)]")
    private WebElement comparePopup;

    @FindBy(xpath = "(//div[@data-lazy-content='compare']//p)[1]")
    private WebElement emptyComparisonItemsCountPopup;

    @FindBy(xpath = "(//a[@data-filter-link and contains(text(), 'Samsung')])[1]")
    private WebElement brandCcheckBox;

    private static final String ITEM_TO_BE_ADDED_TO_COMPARISON_LIST_LOCATOR = "//a[contains(text(), '%s')]/ancestor::li[contains(@class, 'product-item')]//label";

    public FilteredProductsPage(final WebDriver driver) {
        super(driver);
    }

    public void applyFilteringByBrand() {
        getActions().moveToElement(brandCcheckBox).click().build().perform();
    }

    public void addItemsToComparisonList(final String[] itemsToCompare) {
        for (final String item : itemsToCompare) {
            waitForJQuery();
            final WebElement itemToBeAddedToComparisonList = getWebDriver().findElement(xpath(format(ITEM_TO_BE_ADDED_TO_COMPARISON_LIST_LOCATOR, item)));
            getActions().moveToElement(itemToBeAddedToComparisonList).build().perform();
            getWebDriverWait().until(ExpectedConditions.visibilityOf(itemToBeAddedToComparisonList));
            clickElementUsingJavaScript(itemToBeAddedToComparisonList);
        }
    }

    public void clickCompareButton() {
        waitForJQuery();
        scrollPageUntilElementToBeVisibleUsingJavaScript(compareProductButton);
        compareProductButton.click();
    }

    public Integer getItemsCountAddedToCompareList() {
        getActions().moveToElement(comparePopup).build().perform();
        getWebDriverWait().until(ExpectedConditions.visibilityOf(comparePopup));
        final String comparePopupText = comparePopup.getText();
        return Integer.parseInt(comparePopupText.replaceAll("\\D+", ""));
    }

    public ComparisonPage proceedToComparisonPage() {
        scrollPageUntilElementToBeVisibleUsingJavaScript(comparePopup);
        getActions().moveToElement(comparePopup).click().build().perform();
        for (final String windowHandle : getWebDriver().getWindowHandles()) {
            getWebDriver().switchTo().window(windowHandle);
        }
        return new ComparisonPage(getWebDriver());
    }

    public String getEmptyComparisonItemsPopupText() {
        scrollPageUntilElementToBeVisibleUsingJavaScript(compareProductButton);
        getActions().moveToElement(compareProductButton).click().build().perform();
        return StringUtils.trim(emptyComparisonItemsCountPopup.getText());
    }
}

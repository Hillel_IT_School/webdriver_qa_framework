package page_object.core;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ComparisonPage extends BasePage {

    @FindBys({
            @FindBy(xpath = "//div[@class='title-overflow']//a")
    })
    private List<WebElement> addedToComparisonListItems;

    @FindBy(xpath = "//a[@data-clear-compare]//i[@class='delete']")
    private WebElement clearSelectedItems;

    @FindBy(xpath = "//div[@class='lightbox']//button[@data-confirm]")
    private WebElement deleteConfirmationPopup;

    protected ComparisonPage(final WebDriver driver) {
        super(driver);
    }

    public String[] getItemsNameAddedToComparisonList() {
        return addedToComparisonListItems.stream()
                .map(WebElement::getText)
                .map(item -> StringUtils.substringBefore(item, "("))
                .map(StringUtils::trim)
                .toArray(String[]::new);
    }

    public FilteredProductsPage clearSelectedItems() {
        clearSelectedItems.click();
        getWebDriverWait().until(ExpectedConditions.visibilityOf(deleteConfirmationPopup)).click();
        waitForJQuery();
        return new FilteredProductsPage(getWebDriver());
    }
}

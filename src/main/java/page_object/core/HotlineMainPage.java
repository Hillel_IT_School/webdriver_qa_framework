package page_object.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import page_object.common.DefaultUrl;

@DefaultUrl(value = "https://www.hotline.ua")
public class HotlineMainPage extends BasePage {

    @FindBy(xpath = "//li[./a[text()='Смартфоны, Телефоны']]")
    private WebElement menuItem;

    @FindBy(xpath = "//li[@data-level='2' and ./span[text()='Смартфоны, мобильные телефоны, умные часы']]")
    private WebElement menuItemLevelTwo;

    @FindBy(xpath = "//li[@data-level='3' and ./a[text()='Смартфоны и мобильные телефоны']]")
    private WebElement menuItemLevelThree;

    public HotlineMainPage(final WebDriver driver) {
        super(driver);
    }

    public FilteredProductsPage navigateThroughTheSideBarMenu() {
        getActions().moveToElement(menuItem).build().perform();
        getWebDriverWait().until(ExpectedConditions.visibilityOf(menuItemLevelTwo)).click();
        getWebDriverWait().until(ExpectedConditions.visibilityOf(menuItemLevelThree)).click();
        return new FilteredProductsPage(getWebDriver());
    }
}

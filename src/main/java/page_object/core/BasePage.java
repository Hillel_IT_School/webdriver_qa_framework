package page_object.core;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_object.common.DefaultUrl;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public abstract class BasePage {

    private WebDriver webDriver;
    private Actions actions;
    private WebDriverWait webDriverWait;
    private JavascriptExecutor javascriptExecutor;

    private static final Integer DEFAULT_PAGE_TIME_OUT_IN_SECONDS = 30;
    private static final Integer DEFAULT_ELEMENT_TIME_OUT_IN_SECONDS = 30;

    private static final String SCRIPT_PERFORMING_ELEMENT_CLICK = "arguments[0].click()";
    private static final String SCROLL_PAGE_INTO_VIEW_SCRIPT = "arguments[0].scrollIntoView();";
    private static final String SCROLL_PAGE_TO_TOP = "window.scrollTo(0,-1000)";
    private static final String WAITING_FOR_JQUERY_TO_BE_FINISHED = "return !!window.jQuery && window.jQuery.active == 0";

    protected BasePage(final WebDriver driver) {
        this.webDriver = driver;
        actions = new Actions(webDriver);
        webDriverWait = new WebDriverWait(webDriver, DEFAULT_ELEMENT_TIME_OUT_IN_SECONDS);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(DEFAULT_PAGE_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
        javascriptExecutor = ((JavascriptExecutor) webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void clickElementUsingJavaScript(final WebElement webElement) {
        javascriptExecutor.executeScript(SCRIPT_PERFORMING_ELEMENT_CLICK, webElement);
    }

    public void scrollPageUntilElementToBeVisibleUsingJavaScript(final WebElement webElement) {
        javascriptExecutor.executeScript(SCROLL_PAGE_INTO_VIEW_SCRIPT , webElement);
    }

    public void scrollPageToTop() {
        javascriptExecutor.executeScript(SCROLL_PAGE_TO_TOP);
    }

    public void waitForJQuery() {
        (webDriverWait).until((ExpectedCondition<Boolean>) driver -> {
            final JavascriptExecutor js = (JavascriptExecutor) driver;
            return (Boolean) Objects.requireNonNull(js).executeScript(WAITING_FOR_JQUERY_TO_BE_FINISHED);
        });
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public Actions getActions() {
        return actions;
    }

    public WebDriverWait getWebDriverWait() {
        return webDriverWait;
    }

    public JavascriptExecutor getJavascriptExecutor() {
        return javascriptExecutor;
    }

    public <T extends BasePage> void openPage(final Class<T> rootPage) {
        final String defaultUrl = getDefaultUrlInCaseOfNotNullOrEmpty(rootPage);
        navigateToUrl(defaultUrl);
    }

    private void navigateToUrl(final String navigationUrl) {
        webDriver.get(navigationUrl);
    }

    private static <T extends BasePage> String getDefaultUrlInCaseOfNotNullOrEmpty(final Class<T> rootPage) {
        final String defaultUrl = rootPage.getAnnotation(DefaultUrl.class).value();
        if (StringUtils.isBlank(defaultUrl)) {
            throw new IllegalStateException("The page's default url should not be null or empty!");
        }
        return defaultUrl;
    }
}
